// return only the vowels of the string

function vowelsOnly(word) {

    if (typeof (word) !== 'string') {
        return "not a string";
    }
    
    const vowels = /[aeiou]/gi; 
    const newWordArr = word.match(vowels);
    const newWord = newWordArr.join("");
    return newWord;
}

function main() {
    let word = "annyeong";
    let result = vowelsOnly(word);
    console.log(result);

    word = "tic tac toe";
    result = vowelsOnly(word);
    console.log(result);

    word = "hello world";
    result = vowelsOnly(word);
    console.log(result);
}

main()