//STACK LI-FO
class Stack{
    constructor(){
        this.stack = [];
    }

    push(item) {
        this.stack.push(item);
    }

    pop() {
        if (this.stack.length === 0) {
            console.log(`stack is currently empty`);
            return;
        }
        this.stack.pop();
    }

    check() {
        console.log(this.stack);
    }

}

function main() {
    const my_stack = new Stack();
    my_stack.push('Milk'); 
    my_stack.push('Eggs');
    my_stack.check();       //milk, eggs
    my_stack.pop();
    my_stack.check();       //milk
    my_stack.pop();
    my_stack.check();       //empty
    my_stack.pop();         //print stack is empty
    my_stack.push('Bacon'); //bacon
    my_stack.check();
}

main();