
// Queue from scratch - Follows FIFO behaviour 

class Queue{
    constructor(){
        this.queue = [];
    }

    push(item) {
        this.queue.push(item);
    }

    pop() {
        if (this.queue.length === 0) {
            console.log(`Queue is currently empty`);
            return;
        }
        this.queue.shift();
    }

    check() {
        console.log(this.queue);
    }

}

function main() {
    const my_queue = new Queue();
    my_queue.push('Milk'); 
    my_queue.push('Eggs');
    my_queue.check();       //milk, eggs
    my_queue.pop();
    my_queue.check();       //eggs
    my_queue.pop();
    my_queue.check();       //empty
    my_queue.pop();         //print queue is empty
    my_queue.push('Bacon'); //bacon
    my_queue.check();
}

main();