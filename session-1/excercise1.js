```
FizzBuzz

Prints int 1-100
if divisible by 3, Fizz
             by 5, Buzz
             both, FizzBuzz


```

function byThree(number) {
    
    if (number % 3 === 0) {
        return true
    }
    return false
}

function byFive(number) {
    
    if (number % 5 === 0) {
        return true
    }
    return false
}


function main() {
    let both = false
    for (let integer = 1; integer < 101; integer++) {
        both = byThree(integer) && byFive(integer)
        if (byThree(integer) && !both) {
            console.log(`Fizz`)
        } else if (byFive(integer) && !both) {
            console.log(`Buzz`)
        } else if (both){
            console.log(`FizzBuzz`)
        }
        else {
            console.log(integer)
        }
    }
}

main();



