
// Reverse the string given

function reverse(word) {

    if (typeof (word) !== 'string') {
        return "not a string";
    }

    let reverseWord = "";

    for (let character = word.length-1; character >= 0; character--){
        reverseWord += word[character];
    }

    return reverseWord;

}

function reverse2(word) {
    if (typeof (word) !== 'string') {
        return "not a string";
    }

    if (word === "") {
        return word
    }
    return reverse2(word.substring(1)) + word.charAt(0);
}

function reverse3(word) {

    if (typeof (word) !== 'string') {
        return "not a string";
    }

    const listWord = word.split("");
    const reverseList = listWord.reverse();
    const reverseWord = reverseList.join("");

    return reverseWord

}

function main() {
    const word = 1
    let reverseWord = reverse(word);
    reverseWord = reverse2(word);
    reverseWord = reverse3(word);
    console.log(reverseWord);
    
}


main();