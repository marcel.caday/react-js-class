// PALINDROME - returns true, if not false

function reverse(word) {

    
    const listWord = word.split("");
    const reverseList = listWord.reverse();
    const reverseWord = reverseList.join("");

    return reverseWord

}

function isPalindrome(word) {
    if (typeof (word) !== 'string') {
        return false;
    }

    word = word.toLowerCase();
    let reverseWord = reverse(word);

    if (word !== reverseWord) {
        return false;
    }

    return true;



}

function main() {
    let word = "Noon";
    let result = isPalindrome(word);
    console.log(result); //true

    word = "madam";
    result = isPalindrome(word);
    console.log(result); //true
    
    word = "hello";
    result = isPalindrome(word);
    console.log(result); //false

    word = "banana";
    result = isPalindrome(word);
    console.log(result); //false

    
}


main();