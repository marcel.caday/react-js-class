import * as React from "https://cdn.skypack.dev/react@17.0.1";
import * as ReactDOM from "https://cdn.skypack.dev/react-dom@17.0.1";

const {useState} = React
const initialState = 0

const CounterApp = () => {
  const [count, setCount] = useState(initialState)
  const increment = () => setCount(count + 1)
  const decrement = () => {
    if (count === 0) return
    setCount(count - 1)
  }

  return (
    <div>
      <h1>Counter: {count}</h1>
      <button onClick={increment}>Increment</button>
      <button onClick={decrement}>Decrement</button>
    </div>
  )
}


ReactDOM.render(<CounterApp />, document.getElementById('root'))