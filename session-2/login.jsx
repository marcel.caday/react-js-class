import * as React from "https://cdn.skypack.dev/react@17.0.1";
import * as ReactDOM from "https://cdn.skypack.dev/react-dom@17.0.1";

const Header = props => {
  return <h1>{props.message}</h1>
}

const Title = props => {
  return <p className = 'title'>{props.message} : </p>
}

const Button = props => {
  return (
    <div> 
      <button>{props.name}</button>
    </div>
  ) 
}


const InputField = props =>{
  return(
    <div className='inputField'>
      <Title message ={props.field}/>
      <input type={props.type} id= {props.field} />
    </div>
    
  )
}


const Greeting = () =>{
  const user = getElementById('Username').value
  return(
    <div className ='container'> 
      <Header message='Welcome' />
      <Header message= {user}/> 
    </div>
  
  )
}

const redirect = function () {
    const user = getElementById('Username').value
    
}

const Login = () => {
  return (
    <div className='container'>
      <Header message='LOGIN' />
        <InputField field='Username' type= 'text'/>
        <InputField field='Password' type ='password'/>
        <Button name ='Login'/>
    </div>
  )
}



ReactDOM.render(<Login />, document.getElementById('root'))