import * as React from "https://cdn.skypack.dev/react@17.0.1";
import * as ReactDOM from "https://cdn.skypack.dev/react-dom@17.0.1";

const Header = props => {
  return <h1>{props.message}</h1>
}

const GreetingCard = props => {
  return (
    <div className='container'>
      {props.button}
      <Header message='mapping items' />
      {props.children}
    </div>
  )
}

const shoppingList = ['Milk', 'Dog Food', 'Cat Food']
const renderListItems = () => {
  return shoppingList.map((item, index) => {
    const key = `${item}-${index}`
    return <li key={key}>{item}</li>
  })
}

const MyApp = () => {
  return (
    <div>
      <div className='container'>
        <Header message='hello world!' />
        <p>This is my first react code</p>
      </div>
    
      <div>
        <GreetingCard button={<button>Submit</button>}>
          <p>This is my first react code</p>
          <p>This is my first react code - 2</p>
        </GreetingCard>
      </div>
    
      <div>
        <ul>
          {renderListItems()}
        </ul>
      </div>
      
      
    </div>
    
  )
}



ReactDOM.render(<MyApp />, document.getElementById('root'))
