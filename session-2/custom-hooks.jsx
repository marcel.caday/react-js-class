
const { useState } = React

function Button(props) {
  return <button onClick={props.onClick}>{props.label}</button>
}

const useCounter = initialCountValue => {
  const [count, setCount] = useState(initialCountValue)
  const increment = () => setCount(count + 1)
  const decrement = () => {
    if (count === 0) return
    setCount(count - 1)
  }
  
  return {
    count, increment, decrement
  }
}

function Counter(props) {
  const { count, increment, decrement } = useCounter(props.count)
  
  return (
      <div>
        <h1>Counter {props.number}: {count}</h1>
        <Button onClick={increment} label='Increment' />
        <Button onClick={decrement} label='Decrement' />
      </div>  
  )
}

function App() {  
  return (
    <div>
      <Counter number={1} count={0} />
      <Counter number={2} count={10} />
    </div>
  )
}

ReactDOM.render(<App />, document.getElementById('root'))